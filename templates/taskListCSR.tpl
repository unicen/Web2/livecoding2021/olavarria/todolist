{include file='templates/header.tpl'}
<div class="row mt-4">
    <div class="col-md-4">
        {include file='templates/formAlta.tpl'}
    </div>
    <div class="col-md-8">
        <!-- hueco para CSR -->
        {include file="vue/taskListVue.tpl"}
    </div>
</div>

<script src="js/app.js"></script>
{include file='templates/footer.tpl'}
