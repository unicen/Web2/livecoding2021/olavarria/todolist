{literal}
<div id="app">
    <h1>{{ titulo }} </h1>
    <p>{{ subtitulo }}</p>
    
    <ul id="task-list" class="list-group">
        <li v-for="task in tasks" class="list-group-item d-flex">
            {{task.titulo}} | {{task.descripcion}}
            <div class="acciones ms-auto">
                <a class="btn btn-sm btn-danger" v-bind:data-id="task.id">Borrar</a>
                <a v-if="task.finalizada == 0" class="btn btn-sm btn-success" v-bind:data-id="task.id">Done</a>
            </div>
        </li> 

        
    </ul>
</div>
{/literal}
