"use strict"

const API_URL = "api/tareas/";

let app = new Vue({
    el: "#app",
    data: {
        titulo: "Lista de tareas CSR",
        subtitulo: "Esta pagina se renderiza con JS usando Vue.js",
        tasks: [],
    }
});

let form = document.querySelector("form");
form.addEventListener('submit', addTask);

async function getTasks() {
    try {
        let response = await fetch(API_URL);
        let nTasks = await response.json();

        app.tasks = nTasks;
    } catch(e) {
        console.log(e);
    }
}

async function addTask(e) {
    e.preventDefault();
    let data = new FormData(form);
    let task = {
        titulo: data.get('titulo'),
        descripcion: data.get('descripcion'),
        prioridad: data.get('prioridad'),
    }

    try {
        let response = await fetch(API_URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(task),
        });

        if (response.ok) {
            let task = await response.json();
            app.tasks.push(task);
        }

    } catch(e) {
        console.log(e)
    }
}

getTasks();